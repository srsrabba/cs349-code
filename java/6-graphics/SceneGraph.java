/*
* CS 349 Java Code Examples
*
* SceneGraph   Demonstrate simple scene graph.
*
*/

import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;

public class SceneGraph extends JPanel {

    public static void main(String[] args) {
        // create the window
        JFrame f = new JFrame("SceneGraph"); // jframe is the app window
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(400, 400); // window size
        f.setContentPane(new SceneGraph()); // add canvas to jframe
        f.setVisible(true); // show the window
    }

    SceneGraph() {
        setOpaque(true);
        setBackground(Color.WHITE);
    }

    // custom graphics drawing
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.translate(65, 65);
        g2.scale(0.5, 0.5);

        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 6; i++) {
                drawShape(g2);
                g2.translate(110, 0);
            }
            g2.translate(-6 * 110, 110);
        }

        // set transform to identity to reset
        g2.setTransform(new AffineTransform());

        // model to world
        g2.translate(200, 210);
        g2.rotate(Math.toRadians(45));
        g2.scale(1.25, 1.25);
        drawShape(g2);

        // using function
        drawShape(g2, 200, 300, 45, 0.9);
    }


    // draws 100 x 100 window shape centred at 0,0
    void drawShape(Graphics2D g2) {
        g2.setColor(Color.BLACK);
        g2.fillRect(-50, -50, 100, 100);
        g2.setColor(Color.WHITE);
        g2.fillRect(-40, -40, 35, 35);
        g2.fillRect(5, -40, 35, 35);
        g2.fillRect(-40, 5, 35, 35);
        g2.fillRect(5, 5, 35, 35);
    }

    // draws 100 x 100 window shape centred at 0,0
    void drawShape(Graphics2D g2, double x, double y, double theta, double s) {

        // save the current g2 transform matrix
        AffineTransform M = g2.getTransform();

        // set transform to identity to reset
        g2.setTransform(new AffineTransform());

        // do the model to world transformation
        g2.translate(x, y);  // T
        g2.rotate(Math.toRadians(theta)); // R
        g2.scale(s, s);  // S

        drawShape(g2);
        // reset the transform to what it was before we drew the shape
        g2.setTransform(M);
    }
}
